---
author: Osama El Hariri
title: About Me
date: 2023-01-20
description:
keywords: ["about-us", "contact"]
type: about
---


Learning about technologies, then applying this new knowledge to create something and send it off into the world is a great way to give yourself a creative outlet, and to build a stronger portfolio for your career. The truth is, sometimes learning new things is intimidating. With the tech space always expanding rapidly, it can be hard to keep up, but I believe you should have access to resources that explain things clearly. This is what this blog is all about. I take a concept that I run into during the development of my projects, then I get rid of the complexity and focus on one core concept at a time.

My name is Osama El Hariri, and I've been in this space professionally since 2019. I have had the chance to work at different startups, and I know how they work and have a pretty good idea of what it takes to keep a fast pace. I have also had the chance to mentor others, and to watch them begin to grasp concepts that they struggled with. In these blog posts, my goal is to deliver a concept, no matter how complex, right to your doorstep, wrapped in a neat little package, ready to be opened and explored.


<img src="/profile_pic.jpg" alt="My profile picture" class="rounded-lg shadow-sm max-w-md" />

<script async data-uid="52266d6a37" src="https://winning-designer-9314.ck.page/52266d6a37/index.js"></script>
