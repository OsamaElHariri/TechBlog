---
author: "Osama El Hariri"
title: "Tech Co-Founder: My Journey 3 Months In"
date: 2124-02-10
description: "Summarizing my thoughts after being a tech co-founder for 3 months"
tags: ["tech cofounder"]
thumbnail: /blog_headers/buildings_in_the_clouds.jpeg
---

### (Series Intro) My First Tech Co-Founder Journey

While I'm still in the middle of all this, I'd like to write down my thoughts so that I can remember what was going on through my own head, and so that I can share the views I currently hold. Today I am the co-founder of [CleverCue](https://clevercue.app/), a digital product aimed at improving the school dismissal process (where dismissal refers to when the kids are leaving from school at the end of the day). This series is just me jotting down what's in my head.


## How I Started This Journey

Around October 2023, I decided to move on from my full-time job in order to build my own product. This would not be the first time I build a full product by myself, but none of my products have ever "succeeded" from a business standpoint. So I told myself that this next product would be built in a month, then I would start marketing and fishing for PMF (product market fit). After building for a month and a half, I was almost done and ready to start sharing what I've built, but then I was approached by someone who had an idea for a startup. He said that he knows of a school who is looking to solve its dismissal problems, and he knows that they looked at competitors and they were not satisfied with the current offering. There was a gap in the market that we could fill! For context, we are based in Lebanon, and the school is in Saudi Arabia.

I was skeptical, so I told them I need some time to think, but that we should also go around schools in our area to see if a similar problem exists. We drove around, spoke to some schools in Lebanon and to some parents, and I told them I was onboard, as it seems everyone we talk to validates that this problem exists. This made us a team of 3 in total, and we decided that two of us (myself included) will be handling the product development, and one of us will handle sales and marketing.


## Summary of What Was Objectively Done So Far, 3 Months In

- We spoke to schools and parents
- We built a demo in two weeks
- One of us went to Saudi Arabia with this demo and we pitched to a handful of schools
- Now, after the demo, we are talking with one school about starting a pilot
- And we are now heads down building the product so that it is ready for the pilot


## Interesting Challenges When Working in a Team

I have worked in teams at startups for over 5 years, and I've also built products on my own for nearly that long as side-projects, but I have not built a product from zero with a team. 

### The Frontend-Backend Split

The first thought we had was to split the technical work as frontend and backend, where I would take the frontend, and another cofounder would take the backend. I'm a big fan of working fullstack, as I think owning the _entire_ feature is better. You make choices faster, and you are in charge of the full feature, meaning you know exactly what endpoints you need and how you want everything to be structured. Alas, the other cofounder was specialized in the backend, so we went with the frontend-backend split. Looking back at it now, after over a month of development, it ended up not being such a big deal, and the current setup is working fine.

### Choice of Tech Stack

This was interesting. When building my own projects, I had a goal of constantly improving my "project template" so that I could hit the ground running on all fronts, from CI/CD pipelines, to code architecture, to automated tests. This template was React for the web, and Go for the backend. The database would be DynamoDB, and it would be deployed as Lambdas (Note: The backend was *not* microservices. The backend was monolithic, but it was deployed as one Lambda per API route). I was happy with this template, but it fell apart from day 1. My cofounder was familiar with Python and not Go, and since he was in charge of the backend, I didn't push too hard for my template.

Looking back now, there was one sentence that made me want to take charge of the backend. When we decided that we would build the product using Python, my cofounder asked me this: "So what do you want me to do now?". This sentence really stood out to me. It came in stark contrast to what was going on in my head. I was very ready, I had templates, I've built projects from zero many times before, but we decided that I was not responsible for the backend, so I'm not supposed to tell you what should be done next, you should know! That's what was going on in my head at the time, and I'll admit that it's a bit harsh. We did eventually use my templates, but I still remember this whole conversation and I always try to think critically about how I approached it.

After we built the demo using this stack, we ended up dropping DynamoDB in favor of Postgres, and we dropped the Lambda setup in favor of a more standard deployment strategy. So it turned out that my templates were not that great either. I do appreciate that we went through this process though, as it gave me a more solid understanding of the trade offs of a bunch of technologies from a *product team* angle, and not just a technical angle.


### Trusting the Other Person

A while back, I read an article titled "Give Away Your Legos" (I believe it was [this](https://review.firstround.com/give-away-your-legos-and-other-commandments-for-scaling-startups) one). I read it and thought I got it. I know now that I did not get it. I know now what it feels like to give away your Legos. The article talks about giving away what you are working on to someone else so that you can move on to other things. As we were building the product, I got very attached to the backend, as I focused on architecting it in a way that gives us long term speed, as opposed to us writing code haphazardly and slowing down to a halt before we even reach PMF. The cofounder in charge of the backend was making amazing progress and he was starting to own the backend, but I was still stubborn and wanted to still keep an eye on the backend. I believe a turning point for me was when the first set of APIs were done, and he sent me some documentation about how I should call them from the frontend. When I implemented the frontend and called the APIs, everything was working as expected. I remember myself relaxing a little that day. As time went on, my trust in the fact that he's got this has only increased, and I now believe that I understand what it means to give away my Legos.

### Being in a Team Helps Motivation

This is not a challenge, but actually a positive of working in a team. Compared to when I was working on my own projects, founding a company with a team has the benefits of keeping the members motivated. I think all three of us give and take motivation from each other, and it helps that there are things always getting done. If I'm behind, someone else finished something, and so we made progress. If someone else is behind and I finished something, then progress was made. Making progress means things don't get stale, and it means that it is harder to break our forward momentum.



## Final Thoughts

Overall I'm satisfied with what we are doing. Being a co-founder has been a very enriching experience for me, and I'm glad I'm on this journey with people I respect very much, and who I can proudly call my co-founders.





<script async data-uid="52266d6a37" src="https://winning-designer-9314.ck.page/52266d6a37/index.js"></script>