---
author: "Osama El Hariri"
title: "Deciding to Stop Our Startup"
date: 2024-04-25
description: "Reflections on the mistakes we made, and actionable advice on how to avoid them."
tags: ["tech co-founder"]
thumbnail: /blog_headers/stopping_clevercue.png
---


## We Started With Big Dreams

My co-founders and I sat in a meeting room and we discussed what we wanted from this startup. We all wanted to build a company with a great product that made money. Loads of money. Over the last 5 months, we spoke with clients, we built the tech, we spoke with mentors, and we built the tech some more. Today, we're deciding to walk away from this startup, and we'd like to share the decisions we made throughout our journey, along with practical advice that you can use to know if you are repeating some of our mistakes.


## Context on the Team and the Idea

[CleverCue](https://clevercue.app/) is a digital product aimed at improving the school dismissal process (where dismissal refers to when kids leave the school at the end of the day). We are a team of three co-founders who are programmers, [Wajih El Katerji](https://www.linkedin.com/in/wajihelkaterji), [Mohamad Khattab](https://www.linkedin.com/in/mohamadrkhattab), and myself, [Osama El Hariri](https://www.linkedin.com/in/osamaelhariri/). We got the idea of CleverCue when we found a school that was actively checking existing vendors in the market. The school found that the existing vendors did not satisfy its needs. We thought to ourselves: "there is a gap in the market!", and we were off to the races.

![Team lunch. Wajih (Left), Mohamad (Middle), Osama (Right)](/images/stopping_clevercue/team_lunch.jpg)
*Team lunch. Wajih (Left), Mohamad (Middle), Osama (Right)*


Today, we have arrived at the end of our runway. Along the way, we had enough positive signals that gave us confidence that we were on the right track. We had a client that said they were willing to pay, we got compliments from other clients on our initial demo, and parents were all saying that getting their kids from school was cumbersome. There was a lot of nuance in these signals that we did not catch at the time, and we know a lot more now than we did then. We've had many conversations around what we could have done differently. We'll share with you a list of reasons that led us here, and how you can identify if you are headed in a similar direction.

### 1. We Did Not Do Enough Idea Validation

In the first week, we went to schools and we spoke with them about this problem. We spoke with three schools and we spoke with many parents in our network. Our solution was going to solve the disorganization at the school gate, and it was going to make the administrative tasks of the dismissal process easier from inside the school. In hindsight, we can notice that the parents we spoke with were complaining about the traffic jams to get to the schools so they can pick up their kids. Similarly, all the schools we spoke with said that this is a "community problem", which means they also have issues with the traffic jams. This was not what our solution was going to solve. We did have one strong example where a school had a parent who complained that it was uncomfortable for her to walk through a crowd of men at the school gate, get her daughter, then walk back through this crowd of men. We latched onto this example, and we kept building to organize the crowd at the gate assuming that the problem is worth solving.

While we did speak with clients, it was not enough. I watched a [podcast about finding product market fit](https://www.youtube.com/watch?v=yc1Uwhfxacs) recently, and I think the advice in it perfectly applies to us. For B2B SaaS (which is what we were building), you need to find 3 to 5 customers that are willing to pay for your idea before you consider that you're on to something. We had one school that was only _verbally_ onboard, and we know now that this is not enough.


If you think it is hard to find 3 to 5 businesses that are excited to pay for your solution before you build it, you can be 100% sure that trying to sell a product that no one wants is much harder, regardless of how functional it is and how happy you are with your demo. Spend the time on the idea. Pivot early. Get 3 to 5 businesses and actually solve a core problem for them in a way that they are onboard before you even build.

If you stopped speaking with customers and you're already building without having 3 to 5 customers, you're making a mistake.


### 2. We Thought Yes Means Yes

We had one school that said they are onboard, and that they are willing to pay. This is the same school that was looking at the existing vendors and was not satisfied with their offering. We would speak with their IT to ask questions and see how their existing systems work. In hindsight, it turns out they were not really interested in the product we were building, but they were interested in us as a dev team. In one of the meetings with them, we made it clear that we were not interested in developing various products for them, but we are looking to sell this product that we are building, and from then on, our meetings with them became more and more sparse until communication just... stopped.

History did repeat itself here. Recently, we decided to pivot into the nursery space since we happened to get one nursery that was excited to pilot with us. Every nursery we spoke with was not interested in speaking with us because they already had an app, but not this nursery. We even set a date for the pilot. They ended up cancelling the pilot with us because the owner is a programmer and he prefers to implement this in-house.

One conversation I like to refer to is a meeting we had with the school's IT manager about 3 months ago. When we were talking about the vendors they were looking at, we would ask "does this vendor have this functionality" and he would always answer: "yes". We felt a bit defeated and we just asked him why they didn't just go with one of the vendors, since it seems they have everything. With a smile, he said: "Honestly, I don't know...".

In the school's case, we can see they were not interested in paying to solve this problem, as the existing solutions seemed to fit their needs. In the nursery case, they were not interested in solving this problem, since if they did, they would have had an app already. We were not solving a core problem for these businesses.

If your customers said "yes" to you, do not assume they will pay just yet. You can assume your customers will pay when the money is in your bank account. Assuming otherwise is a mistake.


### 3. We Underestimated Our Competition

We researched our competition, and we found that they were all "old", as some of them started over 7 years ago. We also assumed we will offer a richer feature set, with more configurations and flexibility. I've mentioned before that as we met more and more with the IT manager, we found that the competitors actually had a pretty rich feature set. Our differentiators were going to be that we can run this dismissal process offline (without WiFi connection), and that we are more flexible than the competitors. It turns out, running this process offline will cost the school a lot in SMS messages... they would almost be paying more for SMS than for our entire solution. So that differentiator was a hard sell. The second differentiator was not going to happen either, since assuming we could offer more flexibility than a competitor building for 7 years was a bad assumption. In reality, our competitors had an understanding of the problem, the minimal needed effort to solve it given its importance, and what they should price for it. The pricing they were going with was pretty low, and that should have also put a question mark in our minds about the value of solving this problem.

If you are trying to compete with your competitors on number of features or flexibility of the software, know that they have been building much more than you have. You cannot outpace your competition on features, you need a strong differentiator (and no, "cleaner design" is not enough). If you don't have a strong differentiator, you're making a mistake.


### 4. We Built for an Inexpensive User

Our solution has the following main users: The guard at the school gate, the driver or parent that will be picking up the student, and the teachers inside the school. You can imagine that a guard at a school gate does not cost the school too much. Also, the reason we wanted to add an offline mode (which was going to be one of our main differentiators) was because drivers did not have an internet connection. You can also imagine that a school will not be too keen on spending money to make a change in a driver's life. Then you have the teachers, who are in charge of this process after school ends. Teachers usually take turns in deciding who is going to be in the playground on a given day, then they manage this process. For free. Again, you can imagine that a school will not be paying to make a process more efficient if it is already being done for free.

In a business context, a buyer is really looking to answer one simple question before purchasing a product: Will this product decrease my spending, or will this product increase my revenue? If the answer is "this product will do neither", your buyer will not purchase your product. In our case, changing the processes for the guards, drivers, or teachers would not have any effect on the school's balance sheet. Imagine if we said: "we will get you certification X in a fraction of the cost and a fraction of the time". That would have been a tangible business proposition that cuts costs. We should have been looking for something like that, instead of saying something generic like "we will make the dismissal process safer".

If you're building a B2B product that does not enable your clients to spend less money, or make more money, then you're going to have a hard time getting traction, and you're making a mistake.


### 5. We Built Too Much

Throughout our journey, multiple people warned us that we were building too much. Even now, we get comments like "couldn't you have built a smaller version?". Every time I get asked this question, I wonder why I still feel like we could not have built a smaller version. This right here should have been a red flag from the beginning. The reason we could not build a smaller version is because we did not have an "aha! moment", a moment where a user would light up and smile as they get a rush of excitement about what they just experienced. This goes back to us not solving a core problem, and to us not having a strong enough differentiator. Because we defined our differentiator to be: "we are more flexible than our competition", we put ourselves in a bad spot where we had to compete on feature parity with our competitors. Instead, we should have been building towards a single, impressive feature that both, provides value to our clients, and is a feature they will pay for. We should not be building towards a vague comparison like "our system will be more flexible than our competitors".

The lack of an "aha! moment" means a lack of a small version of your product. If you do not define the user journey that will lead to this moment, and you're not building a focused product around this moment, then you're building too much and you're making a mistake.

![Late night dessert](/images/stopping_clevercue/late_night_dessert.jpg)
*Late night dessert*


## On the Bright Side

This journey has been an amazing experience, regardless of the outcome. We were surrounded by other founders that are there to lend us their ear when we wanted to talk about something, founders that gave us introductions when we needed to speak with schools, founders that motivated us when we were down. Going through this journey within a supportive ecosystem like this made all the difference and accelerated our learning by one thousand per cent. Also for me personally, I strongly believe that one thing we got really right is how we treated each other as co-founders. There were conflicts, of course, but we talked through them. There were moments of high pressure and important decisions, and we talked through those as well. We kept things clear and fair between us, and I'll give us a big pat on the back for that!


## Wrapping Up

These are some of the reasons why we had to close up shop. Experienced entrepreneurs would likely recognize all these mistakes, and they would even tell us that this is just the beginning, and that we should stick to it and pivot. While we do acknowledge this feedback, our circumstances are such that we are out of runway and must move on.

Of course, this is not the end of the road for us as entrepreneurs. I like to say this: People say that 9 out of 10 startups fail, but this means that, statistically, we just need to try this 10 times to succeed! We had a lot of discussions with the people around us, and we are grateful to everyone who supported us and pushed us in the right direction. Admittedly, a lot of people gave us sound advice that we should have listened to, but in the heat of things, we kind of underestimated the negative signals and inflated the positive signals. We had to learn these lessons the hard way. This article will hopefully help you avoid these mistakes, and get you on the right track to success 🚀 🚀 🚀


![Everyone](/images/stopping_clevercue/everyone_having_dinner.jpg)
*Thank you to everyone who supported us ❤️! This journey has come to an end, but the next one is just beginning.*




<script async data-uid="52266d6a37" src="https://winning-designer-9314.ck.page/52266d6a37/index.js"></script>