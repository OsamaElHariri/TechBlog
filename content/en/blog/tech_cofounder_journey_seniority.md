---
author: "Osama El Hariri"
title: "Tech Co-Founder: Seniority"
date: 2124-02-24
description: "How to become more senior in your team"
tags: ["tech co-founder"]
thumbnail: /blog_headers/buildings_in_the_clouds.jpeg
---

### (Series Intro) My First Tech Co-Founder Journey

While I'm still in the middle of all this, I'd like to write down my thoughts so that I can remember what was going on through my own head, and so that I can share the views I currently hold. Today I am the co-founder of [CleverCue](https://clevercue.app/), a digital product aimed at improving the school dismissal process (where dismissal refers to when the kids are leaving from school at the end of the day). This series is just me jotting down what's in my head.


## Personal Reflections

Do I consider myself a "senior software engineer"? Throughout my career, I did not. However, today I am able to lead and/or build a technical team that is able to execute well and execute sustainably. I also have confidence in my technical skills, and in my ability to mentor others and to continue to be mentored by others. Today, I am able to sell myself as a senior software engineer. So what changed? What is the difference between me now, and between me a few years ago?

## Experience

The obvious difference is in experience. I simply have more experience now. What does that mean though, and why does that matter? I do not believe that experience comes in years, a person with less years of experience can be more proficient than someone with more years. As the word suggests, more experience means having more, well... "*experiences*". Someone who's seen more, been through more, tried more things. That's experience, and throughout my career, I've made sure to always have new experiences. Whether it is being a part of different teams, taking on different (and more) responsibilities, or being part of companies at different stages and different countries, all these require different approaches to solve their respective problems. Throughout my career, I have collected a good amount of different experiences, and this gives me a large bag of tricks that I can pull from whenever the situation calls for it.

## Collecting Knowledge and Sharing It

I strongly believe that knowledge should be shared. If a mistake is repeated, then that is time and energy that is wasted trying to resolve it again. More than just conversations, knowledge can be shared through observation. I recall being in a team with an engineering manager that is usually calm, but one time had to be strict with someone who was letting things slip and fall behind schedule. It was interesting to see someone who is usually calm and chill, become someone who is firm. Now I have a blueprint of how I could approach situations like this, where I want to be relaxed, but at the same time not allow my team to get into a state where things are slipping through the cracks. I've also observed a Software Architect steer a whole startup to a better place in terms of code quality. The architect would first study the code, then sketch out how things should be, then invite the team to brown bag sessions or other forms of discussions, and slowly but steadily, the whole team started moving to a better architecture, and everyone knew what and _why_ these decisions were made. Observing the way other people interact and approach problems is a great way of collecting knowledge for yourself, as you can learn how to approach things (or how not to approach things, for that matter).

Collecting knowledge helps when the time comes and you need to pull from your bag of tricks in order to solve a problem, but it also makes you better equipped to teach it. I've found that (among other things), sharing knowledge is my go-to way of building teams. The more knowledge I share, the more I am regarded as someone that is worth asking, then I might even be regarded as the "unofficial" senior in the team before getting promoted. This method has worked for me so far: be eager to learn, and be eager to share, but do not share wrong information as that destroys all the trust you've built.

## Owning the Risk

Let's say there is a problem, and around a table people are discussing how to solve it. The person that is able to speak up and say "I will solve this problem by doing X, and I will own the risk of my decision" is the person that will likely be in charge of solving the problem. If that person then delivers on their promise, or at least attempts something and shares the outcome, then that person will be more central to the team in the aftermath (assuming nothing went horribly wrong). Today I am a co-founder of a startup, and many times I've made decisions that I'm ready to take responsibility for. For example, I've pushed back on features that I believe should not be built yet in a pre-product market fit company. I've made decisions on tech stacks and project architectures. I've made decisions on hiring. I own the risk of all these decisions, and this makes me more valuable to the team than if I would have said something like "I'm not sure, you decide" to my co-founders.

Of course I'm not saying we should overwork ourselves by taking on all the decisions, we should pick our battles, but owning some risks and making some decisions that you feel comfortable making is important to grow in your career.

## Confidence

People like people that get things done. Be confident that you can get things done, and convince others that you can get things done. I've personally struggled in owning my achievements and being confident when presenting myself, but I've decided that not being confident is a sign of disrespect towards myself and the work I've done in my career. Today, I am confident that I can get things done.


## Final Thoughts

Naturally, seniority differs across companies and regions. Someone leading a team in a startup has different skills than someone leading a team in enterprise. The same goes for regions, where leading a team in my home country (Lebanon) demands different skills than leading in other countries, but I do believe the core is the same. Sharing knowledge, taking decisions and owning their risk, and being confident are things I found to be common in good leaders across all the teams I've been in.




<script async data-uid="52266d6a37" src="https://winning-designer-9314.ck.page/52266d6a37/index.js"></script>