---
author: "Osama El Hariri"
title: "Tech Co-Founder: Handling Technical Debt in the Idea Stage"
date: 2124-02-18
description: "My approach to technical debt when still in the ideation stage"
tags: ["tech co-founder"]
thumbnail: /blog_headers/buildings_in_the_clouds.jpeg
---

### (Series Intro) My First Tech Co-Founder Journey

While I'm still in the middle of all this, I'd like to write down my thoughts so that I can remember what was going on through my own head, and so that I can share the views I currently hold. Today I am the co-founder of [CleverCue](https://clevercue.app/), a digital product aimed at improving the school dismissal process (where dismissal refers to when the kids are leaving from school at the end of the day). This series is just me jotting down what's in my head.


## Should We Even Care About Tech Debt Right Now???

A common advice floating around is that in the ideation stage of a start-up, the priority is to get something out the door. It should be dirty and scrappy, but it should be good enough. The advice goes as follows: "Why spend the time perfecting the code, when you don't even know if this thing you're building will work?". While this is a fair point, I personally think we should not be this relaxed about code quality, even for the idea stage. I think the trap in this advice is that it frames code quality and speed as being on opposite sides, where if you want one, you trade away the other. I'd like to argue that this is not the case, and that actually putting in some effort on the overall architecture from day one will help you keep moving fast when you reach day 7, then day 30, and so on.

In the ideation stage (and in startups in general), we absolutely should not trade away development speed. If we slow down in the idea stage, our ideas will take longer to build, our motivation will decrease, and before we know it, our idea is just one more incomplete project that we abandoned. Also, slowing down in the idea stage means we can't make aggressive promises to potential clients, and if we can't deliver fast, it defeats the purpose of us being a small, agile team. The insight I'd like to share here is that this applies beyond the first few days, and it applies for the first few months. We absolutely cannot slow down when we are only a few months in. So how can we get to a point where we remain fast even as the project starts spanning months instead of days?

## How to Architect Your Projects Well and Still Move Fast

This section will discuss the methodology that worked for me, and the next section will discuss hands-on technical details.

I'm currently of the opinion that if I write bad code today in one hour, and it slows me down tomorrow by one hour, then I'm better off spending two hours writing good code today, and not having to deal with context switching tomorrow. The effort that must be put early is on these things:

- Overall project architecture
- The building blocks of the project
- Deployment Pipelines

For the overall project architecture, I go for the Clean Architecture (or Port and Adapters/Onion Architecture/etc...). The way we can set this up quickly is by having previous projects in the same tech stack. In my case, I had two previous projects using Clean Architecture in Golang, the second one improving on the architecture of the first one. These projects were complete with APIs and unit tests and business logic. All I had to do to set up our current project was copy and paste, then discuss the architecture with my co-founders. Once this was setup and we were comfortable with the architecture, we did not have to worry about it any more. This saves us time because it introduces standards that are repeatable and predictable. We do not have to waste time thinking about what goes where and where to put which functionalities, all this is clear and we can focus on the business logic we want to ship. Granted, there were times that we had architectural discussions, and those do indeed slow us down, but we believe the trade-off is worth it, and we are still faster because we have this architecture.

For the building blocks, I mean foundational systems that will be used heavily. Things like standard ways of handling errors, or translations, or even how filtering and sorting is handled. These will be used a lot, and it's worth investing in them so that they are properly reusable and we never have to think about them again. The flip side of this is that as we are building a feature, we have to also think about the issues in our error handling, and then we have to fix that in parallel with the feature we are building, which will slow us down and demotivate us. When we see that the building blocks are shaky, we should revisit them on the spot to make sure they get to a point where we do not need to think about them again. Of course, this is within reason, I don't mean we should spend a couple of days on this, I mean if spending at most a day will significantly impact our short-to-mid-term speed in a positive way, we should spend that time.

Finally, for deployment pipelines, this should be figured out early, as empowering ourselves to push to prod from day one allows us to deploy and show our progress in our own web domain. It also means that we are already available for clients to see, and since we are making aggressive promises, this is crucial. It is destructive if we are one week away from some deadline, then along with all the other things we have to deal with, we also have to scramble to figure out deployments since they have been broken for a while. Deployments can be handled in times of calm, and they should be. They should not be handled when times are rough and everything is going downhill.

## Technical Detail and Examples

I'll go into some technical details and give some examples of how putting in effort to improve our code already helped us move faster.

### Project Architecture

In one of the side projects I've built, I've written some details on how I've implemented the Clean Architecture in Go, it can be found [here](https://gitlab.com/OsamaElHariri/greatest-social-network-of-all-time/-/tree/main/golang). Feel free to look around and take inspiration from how it is built.


### The building blocks (Technical Details)

One example I can give that's still fresh in my mind is on the frontend web. It has to do with how modals are implemented. The usual UX we are going for is that you can click on an Edit button, and then a form pops up and you can edit the thing in the modal. I put effort into this until I was able to handle forms like this (this is React code):

```JS
onClick: async () => {
    const modalResult = await modalFormControl.open({
        entityId: entity.Id,
    })
    if (modalResult.status === 'success') {
        refetch()
    }
}
```

The `modalFormControl` variable contains the logic for showing the modal to the user, and the form itself inside the modal has the logic to fetch the entity using the provided entityId, we do not pass in the full entity since that assumes the called has the full entity, we only pass in the ID and the form handles the rest. With this setup, and from anywhere in the code, we can pop-up a modal to edit the entity, then we can do whatever we need in the case of a success.

A few times already, I had to change where things are edited and how things look because of some discussions with my co-founders. We had two separate changes where I needed to add one of our existing modals to a new, complex screen. Both changes combined took me about 5 minutes (including testing, of course). I anticipate a lot more of these types of changes, so putting in the effort to lock down the standard way of doing an edit form in a modal means I do not have to think about this too much when requirements change, and I can ship these kinds of changes in less than 5 minutes.


### Deployment Pipelines

The way we deploy is: We press a button on Gitlab. That's it. Anyone can deploy at any time, and this applies to all our components (frontend and backend). It doesn't matter exactly how it is done, but the benchmark should be that anyone can deploy easily within a few minutes. As an example of why this matters, one of my co-founders was once in a call with a user that is testing our product. A bug popped up. I was able to fix it and deploy it on the spot. This unblocked the user who was testing, and it didn't cost me too much context switching time, since all I had to do was push my code then press a button on Gitlab. (PS: We could make this faster by auto deploying on push, but we went for a manual pipeline where we have to press a button in order to save on pipeline minutes!)


## Final Thoughts

Speed should absolutely not be traded away, but this does not mean we should be collecting technical debt like there is no tomorrow. If we think about technical debt as a debt we are purchasing in order to move faster, we can also think of the practice we do everyday as racking up personal savings. I put in effort in my side projects to figure out how to deploy quickly and how to architect the projects, and that helped us avoid purchasing tech-debt early, since we were "tapping into our personal savings" (I'm trying too hard to keep this analogy going...).

As a small summary, it was really helpful for me to spend time improving how I build projects *before* starting a startup, as it helped in building a stronger foundation than what I would have had otherwise.





<script async data-uid="52266d6a37" src="https://winning-designer-9314.ck.page/52266d6a37/index.js"></script>