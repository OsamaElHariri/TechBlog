---
author: "Osama El Hariri"
title: "Lessons Learned as First-Time Founders"
date: 2124-05-07
description: "Reflections on the mistakes we made, and actionable advice on how to avoid them."
tags: ["tech co-founder"]
thumbnail: /blog_headers/stopping_clevercue.png
---


## Learning From This Journey

In this article, I'll sit my past self down and have an honest conversation with him. My co-founders and I embarked on building [CleverCue](https://clevercue.app/), a digital solution aimed at organizing school dismissal processes (i.e. when kids leave school at the end of the day). As seasoned programmers, we felt equipped to tackle the challenge. However, as we are at the end of our startup runway, I notice that I have a different mindset now than I did 5 months ago when we started, and I'd like to give myself advice that would have put me on a better path.

![Team lunch. Wajih (Left), Mohamad (Middle), Osama (Right)](/images/stopping_clevercue/team_lunch.jpg)
*Us 3 co-founders: [Wajih El Katerji](https://www.linkedin.com/in/wajihelkaterji) (Left), [Mohamad Khattab](https://www.linkedin.com/in/mohamadrkhattab) (Middle), [Osama El Hariri](https://www.linkedin.com/in/osamaelhariri/) (Right)*


### Dear Past Me: Solve a Problem People Will Pay For

Hey, past me, you are a builder that tends to look for an excuse to build something, but make sure you are building something people will pay for. In the next few months, you'll wrongly convince yourself that the problem is worth building for these reasons:

- Parents and schools that you talk to are complaining from the problem
- Your most likely client has the budget to purchase whatever you build
- Your potential clients are paying for competitors

Hidden in these are two main problems.

1. A complaint does not mean a purchase

You know this, you've read this in books like [The Mom Test](https://www.momtestbook.com/), you've heard sayings like "Build a painkiller, not a vitamin". However, you'll think that because your potential client is rich, and because they are already looking for competitor solutions, that means they are willing to pay for your product. Instead of building, ask your clients: "Why did you not purchase a solution already?", or if they have purchased a solution, ask them how they use these tools and what are the problems they still face. You need to know exactly what *they* are looking to achieve, and your solution needs to achieve exactly that.

2. Your solution is not special only because you built it

In your eyes, what you build is valuable purely because of the fact that you built it. In everyone else's eyes, there needs to be a real business differentiator, and you need to be able to convince your clients that you will either make them more money, or save them a lot of money. This goes back to the previous point, you need to know exactly what *they* are looking to achieve. You need to talk with enough clients to internalize their problem. You need to be able to talk about the problem with as much clarity as your clients talk about it. Only then will your insights be sharp enough to build a product that your customers will use and appreciate.


### Dear Past Me: Mind Your Ego

Your ego is good. Everyone needs some ego to believe that they can achieve great things. The problem is when it gets in the way. In the next few months, your ego will unfortunately cause you to underestimate your competitors.

Your competitors are selling their solutions, and they have been in the game for longer than you have, so it's important to keep in mind that they have some things figured out that you do not. Things like how to price your product, how to sell your product, and who to sell it to. Do not try to guess what they're doing right and wrong and assume you only have surface level information, since you are a new player in the game. Ask their clients so you can build an understanding of whether or not your solution is actually doing something different for the users.

### Dear Past Me: Don’t Forget You’re Aiming To Build a Successful Company

Your goal was, and still is, to build a company that is selling a product the market loves. Key words being "selling", "the market", and "loves". You're confident you can build a product people love, but do not forget the other keywords. In the next few months, you will get signals that schools are not really willing to pay for this product, or if they do pay, the amount will be so low, that it is not sustainable or even worth your time. You will also get signals that a lot of schools simply do not have this problem, and this problem is very region specific (in business speak, the TAM is not large). If you doubt that you are building a company that will not achieve your goal, double down on idea validation. Do not be afraid to stop and pivot when you have enough signals to convince you that you are not solving an important enough problem or the market is not big enough.

### Dear Past Me: Beware the Sunk Cost Fallacy

Think of everything you do as an investment. You are investing your time and resources into this company. Always ask yourself and your co-founders if it is still worth it to continue investing in this company. When you ask yourself this question: "Does this company have an ROI that is worth it for me?", the answer should always be "yes". While building our company, we got signals that the price will be too low, and that we need to build a lot for clients to even consider us, but we did not change course because we have already started down this course. You should not be afraid to cut things short, and you should have confidence to tell yourself that your time is valuable and should only be spent building companies with high ROI.


![Late night dessert](/images/stopping_clevercue/late_night_dessert.jpg)
*Late night dessert*


## Wrapping Up

This journey has been an amazing experience, regardless of the outcome. We were surrounded by other founders that are there to lend us their ear when we wanted to talk about something, founders that gave us introductions when we needed to speak with schools, founders that motivated us when we were down. Going through this journey within a supportive ecosystem like this made all the difference and accelerated our learning by one thousand per cent. Also for me personally, I strongly believe that one thing we got really right is how we treated each other as co-founders. There were conflicts, of course, but we talked through them. There were moments of high pressure and important decisions, and we talked through those as well. We kept things clear and fair between us, and I'll give us a big pat on the back for that!


![Everyone](/images/stopping_clevercue/everyone_having_dinner.jpg)
*Thank you to everyone who supported us ❤️! This journey has come to an end, but the next one is just beginning.*




<script async data-uid="52266d6a37" src="https://winning-designer-9314.ck.page/52266d6a37/index.js"></script>